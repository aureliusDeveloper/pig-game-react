<img alt='Pig-game' src='./pig-game-project/src/assets/README-Images/pig-game-logo.svg' width='200px'/>

# Pig game

## Description
***
Pig game is a simple game with two players where each player has to roll a dice and accumulate points in order to win. The first player to reach 100 points wins the game. In order to win the game you will need some luck and strategy! Have fun and enjoy!
## Installation
***

To start the application you have to clone the repository on your computer, go to the folder, containing the `package.json` file and run `npm install` to install all the dependencies that the project needs to become fully functional (that is, if you already have Node.js installed).
After this, you can run the `npm start` to start the game in the browser. Enjoy! 😊

```
npm install
npm start
```

## Features and Usage
***
Initial view when you launch the game
- In each game `Player 1` is always first
- Current active player is indicated by `pink background` and a `dice image` near the player name on top of the player card

![game-start](./pig-game-project/src/assets/README-Images/game-start.png)
***
Start the game
- When `Player 1` rolls the dice the game starts
- Each player can roll the dice as much as they want, but if they get `1` they will lose the accumulated `Current score` and will `LOSE` turn
- You can `HOLD` the accumulated `Current score` and it will be added to your `Total Score` and it will be next player turn

![player1-roll](./pig-game-project/src/assets/README-Images/player1-roll.png)
***
`Player 1` hold their `Current score` right after the first roll and it is added to their `Total Score`
![player1-hold](./pig-game-project/src/assets/README-Images/player1-hold.png)
***
`Player 2` rolls the dice and gets `1`, so his `Current score` is reset to `0` and he loses his turn
![player2-unlucky](./pig-game-project/src/assets/README-Images/player-unclucky-roll.png)
***

## Announcing the winner
***
In this example `Player 1` wins the game, cause he is the one to reach `100` points first
- Click `Play again` button to restart the game

![winner-image](./pig-game-project/src/assets/README-Images/player1-win.png)
***
## Responsiveness

On play `iPhone 12 Pro` 390 x 844

![onPlay-phone-view-image](./pig-game-project/src/assets/README-Images/onPlay-phone-view.png)

On play `iPad Mini` 768 x 1024

![onPlay-phone-view-image](./pig-game-project/src/assets/README-Images/onPlay-tablet-view.png)

On play `iPad Pro` 1024 x 1366

![onPlay-phone-view-image](./pig-game-project/src/assets/README-Images/onPlay-iPadPro-view.png)

On win `iPhone 12 Pro` 390 x 844

![phone-view-image](./pig-game-project/src/assets/README-Images/winner-phone-view.png)

On win `iPad Mini` 768 x 1024

![phone-view-image](./pig-game-project/src/assets/README-Images/winner-tablet-view.png)

On win `iPad Pro` 1024 x 1366

![phone-view-image](./pig-game-project/src/assets/README-Images/onWin-iPadPro-view.png)


## Technologies used
***

- JavaScript
- ReactJS
- HTML
- CSS
- Tailwind
- GitLab
- ESLint
- twin-macro
- styled components

import React from 'react'
/** @jsxImportSource @emotion/react */
import tw from 'twin.macro';

const StyledButton = tw.button`
w-20
cursor-pointer
h-12
bg-rose-500
text-white 
m-2
rounded-md
md:w-16
sm:w-14
`
export const Button = ({title, disabled, eventHandler}) => {
  return (
    <StyledButton disabled={disabled} onClick={eventHandler}>{title}</StyledButton>
  )
}

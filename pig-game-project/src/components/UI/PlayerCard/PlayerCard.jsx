import React from 'react';
/** @jsxImportSource @emotion/react */
import tw, { styled } from 'twin.macro';
import { playersEnum } from '../../../common/enums';
import won from '../../../assets/Images/winner.gif';
import { Button } from '../Button/Button';
import lost from '../../../assets/Images/sad-but-relieved-face.svg';

const PlayerCardWrapper = tw.div`flex
    flex-col
    items-center
    justify-between
    w-[80%] 
    h-[40vh]
    gap-2
    rounded-lg
    shadow-2xl
    bg-slate-200
    border
    sm:w-[40%]
    sm:h-[44vh]
    md:w-[45%]
    md:h-[44vh]
    lg:w-[45%]
    lg:h-[44vh]
    xl:w-[30%]
    xl:h-[44vh]
    `;

const StyledCard = styled(PlayerCardWrapper)(({ isActive }) => [
  isActive && tw`bg-rose-200`,
]);

const PlayerCardSectionWrapper = tw.section`
w-full
h-1/3
flex
flex-col
justify-center
items-center
text-center
gap-4
`;

const ButtonsWrapper = tw.div`
flex
justify-center
`;

const WinnerImage = tw.div`
w-36
`;

const PlayerTurnStyle = tw.h1`
mt-4
text-2xl
font-bold
`;

const TotalScoreStyle = tw.p`
text-rose-500
xl:text-4xl
lg:text-3xl
md:text-2xl
`;


export const PlayerCard = ({
  player,
  playerScore,
  setPlayerScore,
  setDice,
  currentScore,
  setCurrentScore,
  playersTurn,
  setPlayersTurn,
  winner,
  setWinner,
}) => {
  const handleDice = () => {
    setDice(Math.trunc(Math.random() * 6) + 1);
  };

  const handleHoldScore = () => {
    if (playerScore + currentScore >= 100) {
      setPlayerScore((prev) => prev + currentScore);
      setDice(0);
      setCurrentScore(0);
      setWinner(true);
      return;
    } else {
      setPlayerScore((prev) => prev + currentScore);
      setDice(0);
      setCurrentScore(0);
      setPlayersTurn(
        playersTurn === playersEnum.player1
          ? playersEnum.player2
          : playersEnum.player1
      );
    }
  };
  return (
    <StyledCard isActive={playersTurn === player}>
      <PlayerTurnStyle>{`${player} ${
        playersTurn === player ? '🎲' : ''
      }`}</PlayerTurnStyle>
      <TotalScoreStyle>{`Total Score: ${playerScore}`}</TotalScoreStyle>

      {winner === true && playersTurn === player ? (
        <WinnerImage>
          <img src={won} alt="winner-mage" />
        </WinnerImage>
      ) : null}
      {winner === true && playersTurn !== player ? (
        <WinnerImage>
          <img src={lost} alt="winner-mage" />
        </WinnerImage>
      ) : null}
      <PlayerCardSectionWrapper>
        <p className="text-4xl text-rose-500">{`Current score: ${currentScore}`}</p>
        <ButtonsWrapper>
          <Button
            title={'Hold'}
            disabled={playersTurn === player && !winner ? false : true}
            eventHandler={handleHoldScore}
          />
          <Button
            title={'Roll dice'}
            disabled={playersTurn === player && !winner ? false : true}
            eventHandler={handleDice}
          />
        </ButtonsWrapper>
      </PlayerCardSectionWrapper>
    </StyledCard>
  );
};

import dice1 from '../assets/Images/dice-1.png';
import dice2 from '../assets/Images/dice-2.png';
import dice3 from '../assets/Images/dice-3.png';
import dice4 from '../assets/Images/dice-4.png';
import dice5 from '../assets/Images/dice-5.png';
import dice6 from '../assets/Images/dice-6.png';


export const diceImages = Object.freeze([dice1, dice2, dice3, dice4, dice5, dice6]);
/** @jsxImportSource @emotion/react */
import tw from 'twin.macro';
import { PlayerCard } from './components/UI/PlayerCard/PlayerCard';
import { useState } from 'react';
import { playersEnum } from './common/enums';
import { diceImages } from './common/constants';
import { useEffect } from 'react';


const GameWrapper = tw.div`
flex
flex-col
justify-around
items-center
w-full
min-h-screen
bg-slate-500
sm:flex-row
sm:justify-center
sm:gap-2
md:justify-center
md:gap-2
lg:justify-center
lg:gap-2
xl:justify-center
xl:gap-2
`;

const DiceStyledImage = tw.img`
w-20
absolute
m-0
z-40
`;

const PlayAgainButtonStyle = tw.button`
w-20
h-8
mt-1
bg-rose-500
text-white
rounded-md
absolute
cursor-pointer
left-[41.5%]
top-[0%]
sm:left-[47.5%]
sm:top-[16%]
sm:h-12
md:left-[47.5%]
md:top-[16%]
lg:left-[47.5%]
lg:top-[16%]
xl:left-[47.5%]
xl:top-[16%]
`;

function App() {
  const [players, setPlayers] = useState([]);
  const [diceImage, setDiceImage] = useState([]);
  const [player1Score, setPlayer1Score] = useState(0);
  const [player2Score, setPlayer2Score] = useState(0);
  const [dice, setDice] = useState(0);
  const [currentScorePlayer1, setCurrentScorePlayer1] = useState(0);
  const [currentScorePlayer2, setCurrentScorePlayer2] = useState(0);
  const [playersTurn, setPlayersTurn] = useState(playersEnum.player1);
  const [winner, setWinner] = useState(false);

  useEffect(() => {
    setPlayers(Object.values(playersEnum));
  }, []);

  useEffect(() => {
    if (dice > 0) {
      setDiceImage(diceImages);
    }
    if (dice > 1) {
      playersTurn === playersEnum.player1
        ? setCurrentScorePlayer1((prevScore) => prevScore + dice)
        : setCurrentScorePlayer2((prevScore) => prevScore + dice);
    }

    if (dice === 1) {
      playersTurn === playersEnum.player1
        ? setCurrentScorePlayer1(0)
        : setCurrentScorePlayer2(0);
      setPlayersTurn(
        playersTurn === playersEnum.player1
          ? playersEnum.player2
          : playersEnum.player1
      );
    }
  }, [dice]);

  const restartGame = () => {
    setWinner(false);
    setPlayersTurn(playersEnum.player1);
    setDice(0);
    setPlayer1Score(0);
    setPlayer2Score(0);
    setCurrentScorePlayer1(0);
    setCurrentScorePlayer2(0);
  };

  return (
    <GameWrapper>
      {dice > 0 && <DiceStyledImage src={diceImage[dice - 1]} alt="dice" />}
      <PlayAgainButtonStyle onClick={restartGame}>
        Play again
      </PlayAgainButtonStyle>
      {players.map((player) => (
        <PlayerCard
          key={player}
          player={player}
          playerScore={
            player === playersEnum.player1 ? player1Score : player2Score
          }
          setPlayerScore={
            player === playersEnum.player1 ? setPlayer1Score : setPlayer2Score
          }
          dice={dice}
          setDice={setDice}
          currentScore={
            player === playersEnum.player1
              ? currentScorePlayer1
              : currentScorePlayer2
          }
          setCurrentScore={
            player === playersEnum.player1
              ? setCurrentScorePlayer1
              : setCurrentScorePlayer2
          }
          playersTurn={playersTurn}
          setPlayersTurn={setPlayersTurn}
          winner={winner}
          setWinner={setWinner}
        />
      ))}
    </GameWrapper>
  );
}
export default App;
